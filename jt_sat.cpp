#include "jt_sat.h"
#include "ui_jt_sat.h"
/*
 *
 * version .12 started to modify the sat module
 * version .13 added keywords for satellite values
 * version .14 added volume,service change, exit
 * version .15 added keep slate and retrieve slate on start up.
 * version .16 exit satellite service ve exit tuner
 *
 */

// placement of the QObject and QProcess is critical
// it has to be in the include section...continued below
QObject *parent;

QProcess *asrin = new QProcess (parent);

// .....and above this section

jt_sat::jt_sat(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::jt_sat)

{

 homepath = QDir::homePath();

 QString version = "This is Beta version 0.16 of jt-sat.";

 ui->setupUi(this);
 ui->sre_lineEdit->insert("\n");
 ui->sre_lineEdit->insert(version);
 ttsline=version;

 QFile filein1(homepath+"/bos-jt/sat_slate.txt");
  filein1.open(QIODevice::ReadOnly | QIODevice::Text);

  QTextStream streamin1(&filein1);
  while (!streamin1.atEnd()) {
            load_text = streamin1.readLine();
            ui->adapter_lineEdit->insert(load_text);
            load_text = streamin1.readLine();
            ui->freq_lineEdit->insert(load_text);
            load_text = streamin1.readLine();
            ui->sr_lineEdit->insert(load_text);
            load_text = streamin1.readLine();
            ui->service_lineEdit->insert(load_text);
            load_text = streamin1.readLine();
            ui->polar_lineEdit->insert(load_text);
            load_text = streamin1.readLine();
            ui->local_lineEdit->insert(load_text);
 }

 QFile filein(homepath+"/bos-jt/voice.txt");
       filein.open(QIODevice::ReadOnly | QIODevice::Text);

       QTextStream streamin(&filein);
       while (!streamin.atEnd()) {
                 voice = streamin.readLine();
                 voice.prepend(homepath+"/");

      }


 greet();
 num_sr=0;
 num_lo=0;
 num_service=0;
 num_adapter=0;
 num_freq=0;
 num_digit=0;
 num_result=0;
 num_ty=0;


 sre();



}

jt_sat::~jt_sat()
{
    delete ui;
    asrin->kill(); // this is needed to kill instance of julius
                   // other wise there will be too many copies.
}

void jt_sat::sre()
{
   // let's set up julius
    QString confpath = QDir::homePath();
 //   confpath.append("/jt-sat/data/jt-sat.jconf");
    confpath.append("/jt-sat.jconf");  // TODO: Does xxx.jconf have to be in the homepath??

    QString prog = "julius";
    QString conf = "-C";

    QStringList argu;


    argu<<conf<<confpath;


  //  QProcess *asrin = new QProcess(this);
    asrin->setEnvironment(QProcess::systemEnvironment());
    asrin->setProcessChannelMode(QProcess::MergedChannels);

    asrin->start(prog,argu);


    connect(asrin,SIGNAL(readyReadStandardOutput()),this,SLOT(readsre()));
    connect(asrin,SIGNAL(finished(int)),this,SLOT(stopsre()));

}

void jt_sat::readsre()
{
    int foundstart;
    int foundend;

    QString line;
    QByteArray pile;
    QStringList lines;

    QProcess *asrin = dynamic_cast<QProcess *>(sender());


    if(asrin)
    {

       pile=asrin->readAllStandardOutput();
       lines=QString(pile).split("\n");
       foreach (line, lines) {
           if(line.contains("sentence1"))
           {
               foundstart = line.indexOf("<s>")+3;
               foundend = line.indexOf("</s>");
               line.resize(foundend);
               line.remove(0,foundstart);

               asrline=line;
           ui->sre_lineEdit->insert(asrline);
           }

       }
      //  ui->sre_lineEdit->insert(asrin->readAllStandardOutput());

    }
    parse();
}

void jt_sat::stopsre()
{
    ui->sre_lineEdit->clear();
    asrin->waitForFinished();
        if(asrin->state()!= QProcess::NotRunning)
            asrin->kill();
}

void jt_sat::parse()
{

    if(asrline.contains("LOAD")==true)
    {
        asrline.remove(0,5);
        parse_number();
    }

    if(asrline.contains("NUMBER")==true)
    {
        asrline.remove(0,7);
        parse_number();
    }

    if(asrline.contains("PUSH")==true)
    {
        asrline.remove(0,5);
        edit_push();
    }

    if(asrline.contains("APPEND")==true)
    {
        asrline.remove(0,7);
        edit_append();
    }
/*
    if(asrline.contains("NEGATIVE")==true)
    {
        asrline.remove(0,9);
        parse_negative();
    }
*/
    if(asrline.contains("KEEP")==true)
    {
        asrline.remove(0,5);
        save_slate();
    }
    if(asrline.contains("PLAY")==true)
    {
        asrline.remove(0,5);
        play_sat();
    }
    if(asrline.contains("NEXT")==true)
    {
        asrline.remove(0,5);
        change_service();
    }
    if(asrline.contains("EXIT")==true)
    {
        asrline.remove(0,5);
        exit_tuner();
    }
    if(asrline.contains("STORE")==true)
    {
        asrline.remove(0,6); // remove word "STORE"
        parse_store();
    }

    if(asrline.contains("POLARITY")==true)
    {
        asrline.remove(0,9);
        parse_polarity();
    }

    if(asrline.contains("VOLUME")==true)
    {
        asrline.remove(0,7);
        set_volume();
    }
    if(asrline.contains("CLEAR")==true)
    {
        asrline.remove(0,6);
        parse_clear();
    }


    if(asrline.contains("SPEAKER")==true)
    {
        asrline.remove(0,8);
        set_speaker();
    }
    if(asrline.contains("BOS")==true)
    {
        asrline.remove(0,4);
        parse_needs();
    }

}
void jt_sat::parse_number()
{
    num_digit=0;
    num_ty=0;
    QString line = asrline;
    bool ok;


        //line.replace("POINT",".");


        if(line.contains("TWENTY")==true)
        {
           line.remove(0,7);
           num_ty = 20;

        }
        if(line.contains("THIRTY")==true)
        {
           line.remove(0,7);
           num_ty = 30;
        }
/*
        if(line.contains("FOURTY")==true)
        {
            line.remove(0,7);
            num_ty = 40;
        }
        if(line.contains("FIFTY")==true)
        {
           line.remove(0,6);
           num_ty = 50;
        }
        if(line.contains("SIXTY")==true)
        {
           line.remove(0,6);
           num_ty = 60;

        }
        if(line.contains("SEVENTY")==true)
        {
           line.remove(0,8);
           num_ty = 70;
        }
        if(line.contains("EIGHTY")==true)
        {
            line.remove(0,7);
            num_ty = 80;
        }
        if(line.contains("NINETY")==true)
        {
           line.remove(0,7);
           num_ty = 90;
        }

      //     line.replace("POINT",".");
*/
           line.replace("THIRTEEN",QString::number(13));
           line.replace("FOURTEEN",QString::number(14));
           line.replace("FIFTEEN",QString::number(15));
           line.replace("SIXTEEN",QString::number(16));
           line.replace("SEVENTEEN",QString::number(17));
           line.replace("EIGHTEEN",QString::number(18));
           line.replace("NINETEEN",QString::number(19));

           line.replace("ZERO",QString::number(0));
      //     line.replace("OTT",QString::number(0));
      //     line.replace("OH",QString::number(0));
           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));

           line.replace("TEN",QString::number(10));
           line.replace("ELEVEN",QString::number(11));
           line.replace("TWELVE",QString::number(12));



           line.replace(" ","");
           num_digit = line.toFloat(&ok);
           num_result = num_ty + num_digit;
         //  num_result=num_digit;
           line=QString::number(num_result);
           ui->num_lineEdit->clear();
           ui->num_lineEdit->insert(line);


           //l
           //load_time.clear();
          // load_time=line;
           ttsline=line+" has been loaded!";

           greet();
}
void jt_sat::edit_push()
{

    QString push;
    push.clear();
    push=ui->num_lineEdit->text();

    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


           line.replace("ZERO",QString::number(0));

           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));


           line.replace(" ","");
           push.prepend(line);
           num_digit = push.toFloat(&ok);
       //    num_result = num_ty + num_digit;
           num_result=num_digit;

           line=QString::number(num_result);
           ui->num_lineEdit->clear();
           ui->num_lineEdit->insert(line);


           //load_time.clear();
          // load_time=line;
           ttsline=line+" has been loaded!";

           greet();
}

void jt_sat::edit_append()
{

    QString append;
    append.clear();
    append=ui->num_lineEdit->text();

    QString line = asrline;
    bool ok;


        line.replace("POINT",".");


           line.replace("ZERO",QString::number(0));

           line.replace("ONE",QString::number(1));
           line.replace("TWO",QString::number(2));
           line.replace("THREE",QString::number(3));
           line.replace("FOUR",QString::number(4));
           line.replace("FIVE",QString::number(5));
           line.replace("SIX",QString::number(6));
           line.replace("SEVEN",QString::number(7));
           line.replace("EIGHT",QString::number(8));
           line.replace("NINE",QString::number(9));


           line.replace(" ","");
           append.append(line);
           num_digit = append.toFloat(&ok);
           num_result=num_digit;

           line=QString::number(num_result);
           ui->num_lineEdit->clear();
           ui->num_lineEdit->insert(line);


           //load_time.clear();
          // load_time=line;
           ttsline=line+" has been loaded!";

           greet();
}
/*
void jt_sat::parse_negative()
{
    QString line = asrline;
 //   QString result = QString::number(num_result);
    asrline.clear();

//    line.remove(0,6); // remove word "STORE"
    if(line.contains("CHARLIE")==true)
    {
        num_c = num_c * (-1);
        ui->adapter_lineEdit->clear();
        line.clear();
        line = QString::number(num_c);
        ui->adapter_lineEdit->insert(line);
        line.prepend(" The number in Charlie is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("DOG")==true)
    {
        num_freq = num_freq * (-1);
        ui->freq_lineEdit->clear();
        line.clear();
        line = QString::number(num_freq);
        ui->freq_lineEdit->insert(line);
        line.prepend(" The number in DOG is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("ALPHA")==true)
    {
        num_sr = num_sr * (-1);
        ui->sr_lineEdit->clear();
        line.clear();
        line = QString::number(num_sr);
        ui->sr_lineEdit->insert(line);
        line.prepend(" The number in Alpha is now ");
        line.append(" after being multiplied by negative 1.");
    }
    if(line.contains("BOTTOM")==true)
    {
        num_service = num_service * (-1);
        ui->service_lineEdit->clear();
        line.clear();
        line = QString::number(num_service);
        ui->service_lineEdit->insert(line);
        line.prepend(" The number in Bottom is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("BETA")==true)
    {
        num_lo = num_lo * (-1);
        ui->local_lineEdit->clear();
        line.clear();
        line = QString::number(num_lo);
        ui->local_lineEdit->insert(line);
        line.prepend(" The number in Beta is now ");
        line.append(" after being multiplied by negative 1.");
    }

    if(line.contains("TOP")==true)
    {
        num_top = num_top * (-1);
        ui->lineEdit_top->clear();
        line.clear();
        line = QString::number(num_top);
        ui->lineEdit_top->insert(line);
        line.prepend(" The number in Top is now ");
        line.append(" after being multiplied by negative 1.");
    }


    asrline=line;

    greet();

}

*/
void jt_sat::parse_store()
{
    QString line = asrline;
 //   QString result = QString::number(num_result);
    asrline.clear();

    if(line.contains("ADAPTER")==true)
    {
        num_adapter = num_result;
        ui->adapter_lineEdit->clear();
        line.clear();
        line = QString::number(num_result);
        ui->adapter_lineEdit->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Adapter.");
    }

    if(line.contains("FREQUENCY")==true)
    {
        num_freq = num_result;
        ui->freq_lineEdit->clear();
        line.clear();
        line = QString::number(num_result);
        ui->freq_lineEdit->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Frequency.");
    }

    if(line.contains("SYMBOLS")==true)
    {
        num_sr = num_result;
        ui->sr_lineEdit->clear();
        line.clear();
        line = QString::number(num_result);
        ui->sr_lineEdit->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Symbols.");
    }
    if(line.contains("SERVICE")==true)
    {
        num_service = num_result;
        ui->service_lineEdit->clear();
        line.clear();
        line = QString::number(num_result);
        ui->service_lineEdit->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Service.");
    }

    if(line.contains("OSCILLATOR")==true)
    {
        num_lo = num_result;
        ui->local_lineEdit->clear();
        line.clear();
        line = QString::number(num_result);
        ui->local_lineEdit->insert(line);
        line.prepend(" The number ");
        line.append(" has been stored in Oscillator.");
    }



    ttsline=line;

    greet();

}

void jt_sat::parse_polarity()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("HORIZONTAL")==true)
    {
        ui->polar_lineEdit->clear();
        ui->polar_lineEdit->insert("H");

    }
    if(line.contains("VERTICAL")==true)
    {
        ui->polar_lineEdit->clear();
        ui->polar_lineEdit->insert("V");

    }

}

void jt_sat::parse_clear()
{
   QString line = asrline;

   if(line.contains("SLATE")==true)
   {
        clear_boxes();
   }
   if(line.contains("SCRATCH")==true)
   {

       clear_scratch();
    }

}

void jt_sat::clear_boxes()
{
    QString line = asrline;
    if(line.contains("SLATE")==true)
    {
        num_sr=0;
        num_lo=0;
        num_service=0;
        num_adapter=0;
        num_freq=0;
        num_digit=0;
        num_result=0;
        num_ty=0;
        ui->sr_lineEdit->clear();
        ui->local_lineEdit->clear();
        ui->service_lineEdit->clear();
        ui->adapter_lineEdit->clear();
        ui->freq_lineEdit->clear();
        ui->num_lineEdit->clear();
        ui->polar_lineEdit->clear();

        line="The slate has been cleared!";

    }

    ttsline=line;

    greet();

}

void jt_sat::clear_scratch()
{
    QString line = asrline;
    if(line.contains("SCRATCH")==true)
    {
        ui->textEdit->clear();
        line = "Scratch has been cleared!";
    }

    ttsline = line;
    greet();
}

void jt_sat::save_slate()
{
    QString line = asrline;
    asrline.clear();

    if(line.contains("SLATE")==true)
    {
        QFile file(homepath+"/bos-jt/sat_slate.txt");
        if(!file.open (QIODevice::WriteOnly | QIODevice::Text))
            return;

         QTextStream out(&file);

         out<<ui->adapter_lineEdit->text()+"\n";
         line=out.readLine();
         out<<ui->freq_lineEdit->text()+"\n";
         line=out.readLine();
         out<<ui->sr_lineEdit->text()+"\n";
         line=out.readLine();
         out<<ui->service_lineEdit->text()+"\n";
         line=out.readLine();
         out<<ui->polar_lineEdit->text()+"\n";
         line=out.readLine();
         out<<ui->local_lineEdit->text()+"\n";
         line=out.readLine();
         file.close();

         ttsline = "Slate is saved!";

         greet();

    }
}

void jt_sat::parse_needs()
{
    QString line = asrline;
    asrline.clear();


    if(line.contains("WA")==true)
        setup_boswasi();
}

/*
 *void boswasi::date_time()
{
    QString line = asrline;

    ui->boswasi_lineEdit->clear();
    asrline.clear();

    if(line.contains("TIME")==true)
    {

        if(urgent==0)
            ui->boswasi_lineEdit->insert(" The time is, ");
        QString ct = QTime::currentTime().toString("h:mm ap");
        ui->boswasi_lineEdit->insert(ct+". \n");
        ttstemp=ui->boswasi_lineEdit->text();

    }
    else if(line.contains("DATE")==true)
    {

        if(urgent==0)
            ui->boswasi_lineEdit->insert("The date is ");
        QString cd = QDate::currentDate().toString("dddd MMMM dd yyyy");
        ui->boswasi_lineEdit->insert(cd+". \n");
        ttstemp=ui->boswasi_lineEdit->text();
    }
     urgent=0;
     ttsline = ui->boswasi_lineEdit->text();
     greet();
}

 *
*/
void jt_sat::save_scratch()
{
    QString line =asrline;
    QString scratch = "scrtch";
    asrline.clear();

    if(line.contains("SCRATCH")==true)
    {
        QString ct = QTime::currentTime().toString("-h:mm-ap");
        QString cd = QDate::currentDate().toString("-MM-dd-yy");

        QFile file(homepath+"/bos-jt/"+scratch+cd+ct+".txt");
        if(!file.open (QIODevice::WriteOnly | QIODevice::Text))
            return;

         QTextStream out(&file);
         line=out.readLine();
         out<<ui->textEdit->toPlainText();
         file.close();

         ttsline=" math scratch has been saved to the bos-jt folder in your home directory";

         greet();
    }
}

void jt_sat::play_sat()
{
    QString line = asrline;
    asrline.clear();

    // vlc atsc://frequency=623000000 --program=1 --dvb-adapter=1

       QString prog = "vlc";
       QString dvb = "dvb-s2://frequency=";
       QString prog_num = "--program=";
       QString adap_num = "--dvb-adapter=";
       QString budget = "--dvb-budget-mode";
       QString lnblow = "--dvb-lnb-low=9750000";
       QString lnbhi = "--dvb-lnb-high=";
       QString lnbsw = "--dvb-lnb-switch=11700000";
       QString sr ="--dvb-srate=";
       QString pol ="--dvb-polarization=";

       QStringList argu;

       if(line.contains("SATELLITE")==true)
       {

           dvb.append(ui->freq_lineEdit->text()+"000");
           prog_num.append(ui->service_lineEdit->text());
           adap_num.append(ui->adapter_lineEdit->text());
           lnbhi.append(ui->local_lineEdit->text()+"000");
           sr.append(ui->sr_lineEdit->text()+"000");
           pol.append(ui->polar_lineEdit->text());

           if(ui->service_lineEdit->text()=="0")
           {
               argu<<dvb<<adap_num<<lnbhi<<lnblow<<lnbsw<<sr<<pol<<budget;
           }
           else
               argu<<dvb<<prog_num<<adap_num<<lnblow<<lnbhi<<lnbsw<<sr<<pol;

           QProcess *tuner_call = new QProcess(this);

           tuner_call->start(prog,argu);

           ttsline="There you go";
           greet();
       }
}


void jt_sat::greet()
{
    if(ui->ttson_checkBox->isChecked()==true)
        flitetalk();
}







/*
flite: a small simple speech synthesizer
  Carnegie Mellon University, Copyright (c) 1999-2011, all rights reserved
  version: flite-2.0.0-release Dec 2014 (http://cmuflite.org)
usage: flite text/FILE [WAVEFILE]
  Converts text in textFILE to a waveform in WAVEFILE
  If text contains a space the it is treated as a literal
  textstring and spoken, and not as a file name
  if WAVEFILE is unspecified or "play" the result is
  played on the current systems audio device.  If WAVEFILE
  is "none" the waveform is discarded (good for benchmarking)
  Other options must appear before these options
  --version   Output flite version number
  --help      Output usage string
  -o WAVEFILE Explicitly set output filename
  -f textFILE Explicitly set input filename
  -t text     Explicitly set input textstring
  -p PHONES   Explicitly set input textstring and synthesize as phones
  --set F=V   Set feature (guesses type)
  -s F=V      Set feature (guesses type)
  --seti F=V  Set int feature
  --setf F=V  Set float feature
  --sets F=V  Set string feature
  -ssml       Read input text/file in ssml mode
  -b          Benchmark mode
  -l          Loop endlessly
  -voice NAME Use voice NAME (NAME can be filename or url too)
  -voicedir NAME Directory contain voice data
  -lv         List voices available
  -add_lex FILENAME add lex addenda from FILENAME
  -pw         Print words
  -ps         Print segments
  -psdur      Print segments and their durations (end-time)
  -pr RelName Print relation RelName
  -voicedump FILENAME Dump selected (cg) voice to FILENAME
  -v          Verbose mode


*/
void jt_sat::flitetalk()
{
    QString prg = "padsp"; //padsp was added so oss systems will play flite 2.0
    QString prog = "flite";
    QString readfile ="-t";
    QString voicecall = "-voice";

    QString setstuff = "--seti";
    QStringList argu;

   // argu<<voicecall<<voice<<setstuff<<readfile<<ttsline;
    argu<<prog<<voicecall<<voice<<setstuff<<readfile<<ttsline;

    QProcess *flt1 = new QProcess(this);
        flt1->start(prg,argu);
        ttsline.clear();

    sleep(5);
}


void jt_sat::setup_boswasi()
{
        ttsline = "Leaving jt-sat to go to Boswasi! ";
        greet();
        sleep(3);

        QProcess *boswasi_call = new QProcess (this);
        QString prog = "boswasi";

        boswasi_call->startDetached(prog);
        sleep(3);
        QCoreApplication::exit();

}

void jt_sat::set_speaker()
{
    QString line =asrline;
    asrline.clear();


    if(line.contains("OFF")==true)
    {

        ui->ttson_checkBox->setChecked(false);
    }

    else if(line.contains("ON")==true)
    {
        ui->ttson_checkBox->setChecked(true);
    }

    ttsline = "Speaker is ON ";

    greet();

}

void jt_sat::set_volume()
{
    QString line =asrline;
    asrline.clear();

    QString prog = "amixer";
    QString dee = "-D";
    QString pulse = "pulse";
    QString sset = "sset";
    QString master = "Master";
    QString percent = "%";

    QStringList argu;

    if(line.contains("MUTE")==true)
    {
        volume=1;
    }

    else if(line.contains("NOMINAL")==true)
    {
        volume=75;
    }

    else if(line.contains("TOP")==true)
    {
        volume=100;
    }

    else if(line.contains("INCREASE")==true)
    {
        volume=volume+10;
    }

    else if(line.contains("HALF")==true)
    {
        volume=50;
    }
    percent.prepend(QString::number(volume));

    argu<<dee<<pulse<<sset<<master<<percent;

    ui->sre_lineEdit->clear();
    ui->sre_lineEdit->insert("Volume = ");
    ui->sre_lineEdit->insert(percent);


    QProcess *vol_call = new QProcess(this);

    vol_call->start(prog,argu);

   // ttsline = "Volume ";
    ttsline = (ui->sre_lineEdit->text());

    greet();

}
void jt_sat::change_service()
{
    QString prog = "xdotool";
    QStringList argu;
    QString ht_kys = "shift+x";
    QString ky = "key";

    QString line = asrline;
    asrline.clear();

    if(line.contains("SERVICE")==true)
    {
        argu<<ky<<ht_kys;

        QProcess *ch_chnge = new QProcess(this);

        ch_chnge->start(prog,argu);

        ttsline="Changing service";
        greet();

    }

}

void jt_sat::exit_tuner()
{
    QString prog = "xdotool";
    QStringList argu;
    QString ht_kys = "ctrl+q";
    QString ky = "key";

    QString line = asrline;
    asrline.clear();

    if(line.contains("SERVICE")==true)
    {
        argu<<ky<<ht_kys;

        QProcess *ch_chnge = new QProcess(this);

        ch_chnge->start(prog,argu);

        ttsline="Exiting the satellite service!";
        greet();

    }


}
