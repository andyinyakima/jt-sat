#ifndef JT_SAT_H
#define JT_SAT_H

#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QFileSystemWatcher>
#include <QDate>
#include <QProcess>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <unistd.h>
#include <qmath.h>
//#include <math.h>


namespace Ui {
class jt_sat;
}

class jt_sat : public QMainWindow
{
Q_OBJECT

public:
explicit jt_sat(QWidget *parent = 0);
~jt_sat();

private slots:

void sre();

void readsre();

void stopsre();

void parse();

void parse_number();

void edit_push();

void edit_append();

//void parse_negative();

void parse_store();

void parse_polarity();

void parse_clear();

void clear_boxes();

void clear_scratch();

void save_slate();

void parse_needs();

void save_scratch();

void play_sat();

void greet();

void flitetalk();

void set_speaker();

void setup_boswasi();

void set_volume();

void change_service();

void exit_tuner();

private:
int volume;

int num_result;
int num_sr;
int num_lo;
int num_adapter;
int num_freq;
int num_service;
int num_digit;
int num_ty;
int num_edit;



QString homepath;
QString asrline;  // SRE in
QString ttsline;  // tts out
QString voice;
QString load_text;
QString polarization;





Ui::jt_sat *ui;
};

#endif // JT_SAT_H



